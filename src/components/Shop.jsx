import React, {useState} from 'react';
import Filter from './Filter';
import Productos from './Productos';

const Shop = () => {
    const [term, setTerm] = useState('');
    const [currentSearchValue,setCurrentSearchValue] = useState('')

    const handleChange = event => setTerm(event.target.value)
    const handleSubmit = (e) => {
        e.preventDefault()
        setCurrentSearchValue(term)
    }

    return (
        <div className="ui grid" style={{margin: "30px",paddingLeft: "10px"}} >
				<div className="three wide column" >
					<Filter handleChange={handleChange} handleSubmit={handleSubmit} />
				</div>
				<div className="thirteen wide column" >
                    <Productos brand={currentSearchValue}/>
				</div>
		</div>
    )
}

export default Shop
