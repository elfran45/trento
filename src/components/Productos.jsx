import React, {useState, useEffect} from 'react';
import {db} from '../firebase';
import Card from './Card'

import  './Products.css'

const Productos = ({brand}) => {

  const [productos, setProductos] = useState([]);

  useEffect(() => {
    db.collection('productos').onSnapshot(snapshot => {
      setProductos(snapshot.docs.map(doc => ({
        id: doc.id,
        data: doc.data()
      })))
    })
  }, []); 

  const renderCard = (producto) => {
    return (
      <Card           key={producto.id} 
                      img={producto.data.img} 
                      nombre={producto.data.nombre} 
                      descripcion={producto.data.descripcion} 
                      precio={producto.data.precio} 
                      stock={producto.data.stock} 
                    />
    )
  }

    
    return (
        <div className="ui grid container stackable">
            {productos.map((producto) => {
               if(producto.data.marca === brand){
                
                return renderCard(producto)
              } else if(!brand){
                return renderCard(producto)
              }
             })
            }
        </div>
    )

    
}

export default Productos
        