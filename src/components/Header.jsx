import React from 'react';
import './Header.css'

const Header = () => {
    return (
        <div className="main-header">
            <div className="left-menu">
                <a href="/">
                <div className="logo">
                    <h3>Trento</h3>
                    <h4>Guitar Shop</h4>
                </div>
                </a>
                
            </div>
            <div className="right-menu">
                <div className="nav">
                    <ul>
                    <li><a href="#marcas"><i className="cart icon"></i></a></li>
                        <li><a href="/shop">Shop</a></li>
                        <li><a href="#marcas">Brands</a></li>
                        <li>Contact Us</li>
                    </ul>
                </div>
                <div className="user">
                    <button className="log-button" >Login</button>
                    <button className="sign-button" >Signin</button>
                </div>
            </div>
        </div>
    )
}

export default Header
