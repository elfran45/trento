import React, {useEffect, useState} from 'react';

import {db} from '../firebase';
import './MarcasCards.css'

const MarcasCards = () => {

    const [logos, setLogos] = useState([])

    useEffect(() => {
    db.collection('logos').onSnapshot(snapshot => {
        setLogos(snapshot.docs.map(doc => ({
          id: doc.id,
          logo: doc.data()
        })))
      })
    }, []);
    
    

    return (
        <div id="marcas" className="logos-grid" >
            {
                logos.map((logo) => {
                    return(
                            <div key={logo.id} className="logos-grid-card">
                                <img className="logos-grid-card-image" src={logo.logo.img} alt=""/>
                            </div>
                        )
                })
            }
        </div>
    )
}

export default MarcasCards;