import React from 'react';
import './MainPage.css';
import MarcasCard from './MarcasCards';

const MainPage = () => {
    return (
        <div className="main">
            <div className="greeting" >
            <div className="greet-box">
                <h4>DISCOVER YOUR NEW <br/> SOUND</h4>
                <p>Here at <strong>Trento</strong> we know how important of a tradition is to find a new guitar.
                That’s why we provide you with the best possible enviroment to choose the guitar of your dreams.
                </p>
                <button>Contact Us</button>
            </div>
            </div>
            <MarcasCard />
        </div>
        
    )
}

export default MainPage
