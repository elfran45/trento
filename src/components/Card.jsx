import React from 'react'

const Card = ({id, nombre, descripcion, img, precio, stock}) => {

    const renderStock = () => {
      if(stock > 0){
        return `Available In Stock: ${stock}`
      } else{
        return 'No Stock'
      }
    }
    

    return (
      <div className="four wide column" >
        <div key={id} className="ui card">
            <div className="img-container">
              <img height="300" src={img} />
            </div>
            <div className="content">
              <a className="header">
                <h4>{nombre}</h4>
              </a>
              <div className="meta">
                <span className="date"> {renderStock()} </span>
              </div>
              <div className="description">
                
              </div>
            </div>
            <div className="extra content">
              <a>
                <i className="cart icon"></i>
                {`US$ ${precio}`}
              </a>
            </div>
        </div>
      </div>
    )
}

export default Card
