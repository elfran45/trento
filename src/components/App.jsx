import React from 'react';
import {Route, BrowserRouter as Router} from 'react-router-dom';

import './App.css'
import Header from './Header'
import Shop from './Shop';
import MainPage from './MainPage';

const App = () => {

	return (
        <div className="App">
			<Router>
				<Header />
				<Route path="/" exact component={MainPage} />
				<Route path="/shop" component={Shop} />
			</Router>
		</div>
    )
};

export default App;
        