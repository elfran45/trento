import React from 'react'

const Filter = ({handleChange, handleSubmit}) => {
    return (
        <div className="ui vertical text menu">
            <form onSubmit={handleSubmit} className="ui icon input">
            	<input onChange={handleChange} className="prompt" type="text" placeholder="Search..."/>
				<i className="search icon"></i>
            </form>
			
			<div className="header item">Sort By</div>
			<a className="active item">
				Brand
			</a>
			<a className="item">
				Strings
			</a>
			<div className="ui divider"></div>
            <div className="header item">Price</div>
			<a href="" className="active item">
				US$500 - US$750
			</a>
			<a href="" className="item">
				US$750 - US$1500
			</a>
			<a href="" className="item">
				US$1500 - ...
			</a>
            <div className="ui divider"></div>
            <div className="header item">Brand</div>
			<a href="" className="active item">
				Martin
			</a>
			<a href="" className="item">
				Taylor
			</a>
			<a href="" className="item">
				Ovation
			</a>
			<a href="" className="item">
				Yamaha
			</a>
			<a href="" className="item">
				Gibson
			</a>
			<a href="" className="item">
				PRS
			</a>
		</div>
    )
}

export default Filter
